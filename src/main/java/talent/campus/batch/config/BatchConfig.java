package talent.campus.batch.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import talent.campus.batch.domain.User;
import talent.campus.batch.items.FilterProcessor;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

	@Bean
	public FlatFileItemReader<User> reader() {
		return new FlatFileItemReaderBuilder<User>().name("reader_builder")
				.resource(new ClassPathResource("sample-data.csv")).targetType(User.class).delimited().delimiter(",")
				.names("fullName", "age", "email").build();
	}

	@Bean
	public JdbcBatchItemWriter<User> writer(DataSource ds) {
		return new JdbcBatchItemWriterBuilder<User>().dataSource(ds)
				.sql("INSERT INTO user (name, age, email) VALUES (:fullName,:age,:email)").beanMapped().build();
	}

	@Bean
	public Step step(StepBuilderFactory stepBuilderFactory, ItemReader<User> reader, ItemWriter<User> writer ) {
		return stepBuilderFactory.get("load-csv-user")
				.<User,User>chunk(100)
				.reader(reader)
				.writer(writer)
				.processor(new FilterProcessor())
				.build();
	}

	@Bean
	public Job job(JobBuilderFactory jobBuilderFactory, Step step) {
		return jobBuilderFactory.get("etl-1").incrementer(new RunIdIncrementer()).start(step).build();
	}
}
