package talent.campus.batch.items;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;

import talent.campus.batch.domain.User;

public class FilterProcessor implements ItemProcessor<User, User> {

	private Long ageGreaterThen;
	
	@BeforeStep public void beforeStep(final StepExecution stepExecution) { 
		JobParameters jobParameters = stepExecution.getJobParameters();
		this.ageGreaterThen = jobParameters.getLong("ageGreaterThen", Long.valueOf(0));
	}
	
	@Override
	public User process(User item) throws Exception {	
		if(item.getAge() > this.ageGreaterThen) {
			System.out.println(item.toString());
			return item;
		}
		return null;
	}

}
