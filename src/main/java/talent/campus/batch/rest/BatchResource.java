package talent.campus.batch.rest;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import talent.campus.batch.exception.InternalErrorException;

@RestController
@RequestMapping("/api/batch/")
public class BatchResource {

	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private Job job;
	
    @PostMapping("start/")
    public ResponseEntity<Void> start() {

    	JobParameters jobParameters = new JobParametersBuilder()
    			.addLong("ageGreaterThen", Long.valueOf(34))
    			.toJobParameters(); 
    	try {
			this.jobLauncher.run(this.job, jobParameters);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException e) {
			e.printStackTrace();
			throw new InternalErrorException("Error starting job: " + e.getMessage(), e);
		}
    	
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
