package talent.campus.batch.domain;

public class User {

	private String fullName;
	
	private Long age;
	
	private String email;

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getAge() {
		return this.age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [fullName=" + this.fullName + ", age=" + this.age + ", email=" + this.email + "]";
	}
	
	
}
